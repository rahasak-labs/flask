# gateway

dockerized flask gateway service


## build docker

the gateway service serves simple flask rest api. service can be dockerized
with below command. 

```
docker build --tag erangaeb/flask-gateway:0.1 .
```


## run docker

the rest api servers in port 7654. following is the way to run service with
docker

```
docker run -p 7654:7654 erangaeb/flask-gateway:0.1
```


## test apis

`gateway` exposes GET and POST rest endpoints for `api/indentities`. these apis
can be test with `curl`

### 1. get identity

```
# request
curl -i -XGET 'http://192.168.8.101:7654/api/identities'

# reply
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 52
Server: Werkzeug/2.0.1 Python/3.8.11
Date: Tue, 20 Jul 2021 04:52:41 GMT

{
  "address": "suffolk va",
  "name": "rahasak"
}
```

### 2. post identiy

```
# request
curl -i -XPOST "http://localhost:7654/api/identities" \
--header "Content-Type: application/json" \
--data '
{
  "name": "bassa",
  "address": "norfolk va"
}
'

# reply
HTTP/1.0 201 CREATED
Content-Type: application/json
Content-Length: 41
Server: Werkzeug/2.0.1 Python/3.8.11
Date: Tue, 20 Jul 2021 04:55:19 GMT

{
  "msg": "created",
  "status": 201
}
```
