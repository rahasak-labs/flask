from flask import Flask
from flask import jsonify
from flask import request
import logging

app = Flask(__name__)

@app.route('/api/identities', methods=['GET'])
def getIdentity():
    data = {'name':'rahasak', 'address':'suffolk va'}
    app.logger.info("get identity response %s", data)

    return jsonify(data), 200

@app.route('/api/identities', methods=['POST'])
def postIdentity():
    identity = request.get_json(silent=True)
    app.logger.info("post identity request %s", identity)

    data = {'status': 201, 'msg': 'created'}
    return jsonify(data), 201

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7654, debug=True)
